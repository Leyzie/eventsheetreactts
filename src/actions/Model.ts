import { Actions } from "./Actions";

//  Типы данных для пользователя
export interface IUser {
    avatar: string;
    date: string;
    id: string;
    name: string;
}
// Данные одного комментария
export interface ICommentsUser {
    avatar: string;
    date: string;
    eventId: string;
    id: string;
    name: string;
    text: string;
}
// Данные общие
export interface IPostDate {
    comments?: ICommentsUser[];
    description: string;
    dete: string;
    id: string;
    important: boolean;
    title: string;
    user: IUser[];
    userId: string;
}

// Стейт список событий
/**
 * Состояние для Redux хранилища (стора).
 * @prop {IPostDate[]} eventList Массив событий.
 * @prop {boolean} loading Ожидание завершения процедуры получения данных (завершение получение данных).
 * @prop {string} error Ошибки.
 */
export interface IStoreEvent{
    eventList: IPostDate[];
    loading: boolean;
    error: string;
}

// Стейт Одного события
/**
 * Состояние для Redux хранилища (стора).
 * @prop {IPostDate[]} oneEvent Одно событие.
 * @prop {boolean} loading Ожидание завершения процедуры получения данных (завершение получение данных).
 * @prop {string} error Ошибки.
 */
export interface IStoreOne{
    oneEvent: IPostDate[];
    loading: boolean;
    error: string;
}
/**
 * Общий стор для Redux 
 */
export interface IStoreState{
    event: IStoreEvent,
    oneEvent: IStoreOne
}

/**
 * Пропсы для передачи экшенов.
 * @prop {Actions} actions Экшены для работы приложения.
 */
export interface IDispatchProps{
    actions: Actions;
}