import { Dispatch } from 'redux';
import { IActionType } from '../common';
import { ActionTypes, AsyncActionTypes } from './Consts';

interface IDataEvent {
    description: string;
    dete: string;
    important: boolean;
    title: string;
}
/**
 * Основной экшен
 */
export class Actions {

    constructor(private dispatch: Dispatch<IActionType>) {
    }
    // Получить событие
    getAllPost() {
        this.dispatch({type: `${ActionTypes.GETEVENT}${AsyncActionTypes.BEGIN}`});
        const options = {
            method: 'GET',
            headers: {
                'Content-Type': 'application/json;charset=utf-8',
            },
        }
        fetch(`http://5db050f78087400014d37dc5.mockapi.io/api/users/9/events`, options)
        .then((res) => {
            if(res.ok){
                res.json().then((data) => {
                    // console.log(data)
                    this.dispatch({
                        type: `${ActionTypes.GETEVENT}${AsyncActionTypes.SUCCESS}`,
                        payload: data
                    })
                })
            }
        })
        .catch((err) => {
            this.dispatch({type: `${ActionTypes.GETEVENT}${AsyncActionTypes.FAILURE}`, payload: err});
        });
    }
    // Получить события
    getOnePost(id: string) {
        this.dispatch({type: `${ActionTypes.GETONEEVENT}${AsyncActionTypes.BEGIN}`});
        const options = {
            method: 'GET',
            headers: {
                'Content-Type': 'application/json;charset=utf-8',
            },
        }
        fetch(`http://5db050f78087400014d37dc5.mockapi.io/api/users/9/events/${id}`, options)
        .then((res) => {
            if(res.ok){
                res.json().then((data) => {
                    this.dispatch({
                        type: `${ActionTypes.GETONEEVENT}${AsyncActionTypes.SUCCESS}`,
                        payload: data
                    })
                })
            }
        })
        .catch((err) => {
            this.dispatch({type: `${ActionTypes.GETONEEVENT}${AsyncActionTypes.FAILURE}`, payload: err});
        });
    }
    // Добавить событие
    addEvent = (DataEvent: IDataEvent) => {
        const options = {
            method: 'POST',
            body: JSON.stringify(DataEvent),
            headers: {
                'Content-Type': 'application/json;charset=utf-8',
            },
        }
        fetch(`http://5db050f78087400014d37dc5.mockapi.io/api/users/9/events`, options)
        .then((res) => {
            console.log(res);
            if(res.ok){
                res.json().then((data) => {
                    // console.log(data)
                    console.log(data);
                })
            }
        })
        .catch((err) => {
            console.log(err);
        });
    }
    // удалить события
    delEvent(id: string) {
        const options = {
            method: 'DELETE',
            headers: {
                'Content-Type': 'application/json;charset=utf-8',
            },
        }
        fetch(`http://5db050f78087400014d37dc5.mockapi.io/api/users/9/events/${id}`, options)
        .then((res) => {
            if(res.ok){
                res.json().then((data) => {
                    console.log(data);
                })
            }
        })
        .catch((err) => {
            console.log(err);
        });
    }
}