/**
 * Типы экшенов, используемые в приложении.
 * GETEVENT - Получить список события.
 * GETONEEVENT - Получить одно событие
 */
export enum ActionTypes {
    GETEVENT = 'ACTION_GETEVENT',
    GETONEEVENT = 'ACTION_GETONEEVENT',
}

/**
 * Подтипы для экшенов при ассинхронной работы.
 * BEGIN - Начало ассинхронного действия.
 * SUCCESS - Действие завершилось успешно.
 * FAILURE - Действие завершилось с ошибкой.
 */
export enum AsyncActionTypes {
    BEGIN = '_BEGIN',
    SUCCESS = '_SUCCESS',
    FAILURE = '_FAILURE',
}
