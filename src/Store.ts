import {applyMiddleware, createStore} from 'redux';
import {composeWithDevTools} from 'redux-devtools-extension';
import thunk from 'redux-thunk';
import rootReducer from './reducers';

/**
 * Redux хранилище (стор).
 */
const store = createStore(rootReducer, composeWithDevTools(applyMiddleware(thunk)));

export {store as appStore};
