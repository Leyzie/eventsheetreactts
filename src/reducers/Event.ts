import {IActionType} from '../common';
import {ActionTypes, AsyncActionTypes} from '../actions/Consts';
import { IStoreEvent } from '../actions/Model';

/**
 * Начальное состояние стора.
 */
const initialState = {
    get state(): IStoreEvent {
        return {
            eventList: [],
            loading: false,
            error: ''
        };
    }
};

export function event(state: IStoreEvent = initialState.state, action: IActionType) {
    switch (action.type) {
        case `${ActionTypes.GETEVENT}${AsyncActionTypes.BEGIN}`:
            return {
                ...state,
                eventList: [],
                loading: true,
                error: 'Загрузка'
            };

        case `${ActionTypes.GETEVENT}${AsyncActionTypes.SUCCESS}`:
            return {
                ...state,
                eventList: action.payload,
                loading: false,
                error: 'Загрузка завершина'
            };

        case `${ActionTypes.GETEVENT}${AsyncActionTypes.FAILURE}`:
            return {
                ...state,
                eventList: [],
                loading: false,
                error: action.payload
            };
    }
    return state;
}
