import { combineReducers } from "redux";
import { event } from './Event'
import { oneEvent } from "./OneEvetn";

const rootReducer = combineReducers({
    event: event,
    oneEvent: oneEvent
})

export default rootReducer