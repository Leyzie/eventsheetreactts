import {IActionType} from '../common';
import {ActionTypes, AsyncActionTypes} from '../actions/Consts';
import { IStoreOne } from '../actions/Model';

/**
 * Начальное состояние стора.
 */
const initialState = {
    get state(): IStoreOne {
        return {
            oneEvent: [],
            loading: true,
            error: 'Загрузка'
        };
    }
};

export function oneEvent(state: IStoreOne = initialState.state, action: IActionType) {
    switch (action.type) {
        case `${ActionTypes.GETONEEVENT}${AsyncActionTypes.BEGIN}`:
            return {
                ...state,
                oneEvent: [],
                loading: true,
                error: 'Загрузка'
            };

        case `${ActionTypes.GETONEEVENT}${AsyncActionTypes.SUCCESS}`:
            return {
                ...state,
                oneEvent: action.payload,
                loading: false,
                error: 'Загрузка завершина'
            };

        case `${ActionTypes.GETONEEVENT}${AsyncActionTypes.FAILURE}`:
            return {
                ...state,
                oneEvent: [],
                loading: false,
                error: action.payload
            };
    }
    return state;
}
