import React from 'react'
import { Link } from 'react-router-dom'

interface Props {
    
}

const ControlsBtn: React.FC<Props> = () => {
    return (
      <div className="container">
        <div className="row jcsb">
          <Link to="/addevent" className="btn btn-green">+ Добавить событие</Link>
          <button className="btn ">Сортировать по: Дате</button>
        </div>
      </div>
    )
}

export default ControlsBtn
