import React, { Component } from 'react'
import { IPostDate } from '../actions/Model'
import { Link } from 'react-router-dom'

interface IDelACtion{
    delete: Function;
}

type IProps = IPostDate & IDelACtion

export default class EventList extends Component<IProps> {
    
      
    getDate = (value: string) => {
        let options = {
            day: 'numeric',
            month: 'numeric',
            year: 'numeric'
        }
        var date = new Date(value);
        return date.toLocaleString('ru', options)
    }

    render() {
        const { comments, description, dete, title, id } = this.props
        return (
            <div className="post link">
                <div className="post_head">
                    <Link className="title" to={`/event/:${id}`}>
                        {title}
                    </Link>
                    <button className="del" >Удалить</button>
                </div>
                <div className="post_body">
                    <p>
                        {description}
                    </p>
                </div>
                <div className="post_footer">
                    <div className="date">
                        Дата проведения:  {this.getDate(dete)}
                    </div>
                    <div className="comments">
                        Комментарии: {comments === undefined ||  comments.length <= 0? 0 : comments.length+1}
                    </div>
                </div>
            </div>
        )
    }
}
