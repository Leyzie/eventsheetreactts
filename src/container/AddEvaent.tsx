import React, { Component } from 'react'

interface Props {
    
}
interface State {
    title: string;
    desc: string;
    date: string;
    error: string;
}

export default class AddEvaent extends Component<any, State> {

    constructor(props: any) {
        super(props);
        this.state = {title: '',desc: '',date: '',error: ''}
        
        this.titleOncehnge = this.titleOncehnge.bind(this);
        this.descOncehnge = this.descOncehnge.bind(this);
        this.dateOncehnge = this.dateOncehnge.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
    }
    titleOncehnge(e: React.ChangeEvent<HTMLInputElement>)  {
        this.setState({title: e.target.value});
    }
    descOncehnge(e: React.ChangeEvent<HTMLTextAreaElement>)  {
        this.setState({desc: e.target.value});
    }
    dateOncehnge(e: React.ChangeEvent<HTMLInputElement>)  {
        this.setState({date: e.target.value});
    }
    handleSubmit = (e: React.FormEvent<HTMLFormElement>) => {
        e.preventDefault();
        const { title, desc, date } = this.state
        if(title === ''|| desc === '' || date === ''){
            return this.setState({error: 'Заполните все поля в форме'})
        }
        console.log(`Название ${title} -- Описание ${desc} -- Дата ${date}`);
    };
    render() {
        return (
            <div className="container" >
                <div className="row">
                    <h1>Создание мероприятия</h1>
                    <form className="form" onSubmit={this.handleSubmit}>
                      <div className="form-group">
                          <label htmlFor="title">Заголовок:</label>
                          <input id="title" name="title" type="text" className="input-text" placeholder="Заголовок..." value={this.state.title} onChange={this.titleOncehnge}/>
                      </div>
                      <div className="form-group">
                          <label htmlFor="desc">Описание:</label>
                          <textarea name="desc" id="desc" className="textarea" placeholder="Описание..." value={this.state.desc} onChange={this.descOncehnge}></textarea>
                      </div>
                      <div className="form-group">
                          <label htmlFor="date">Дата:</label>
                          <input id="date" name="date" type="date" className="input-date" placeholder="Заголовок..." value={this.state.date} onChange={this.dateOncehnge}/>
                      </div>
                      <p>{this.state.error}</p>
                      <button type="submit" className="btn btn-green">Опубликовать</button>
                    </form>
                </div>
            </div>
        )
    }
}
