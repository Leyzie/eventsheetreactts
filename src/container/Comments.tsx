import React, { Component } from 'react'
import { ICommentsUser } from '../actions/Model'
import { Link } from 'react-router-dom'

interface IDelACtion{
    delete: Function;
}

type IProps = ICommentsUser & IDelACtion

export default class Comments extends Component<IProps> {
    
      
    getDate = (value: string) => {
        let options = {
            day: 'numeric',
            month: 'numeric',
            year: 'numeric'
        }
        var date = new Date(value);
        return date.toLocaleString('ru', options)
    }

    render() {
        const { avatar, date, name, text } = this.props
        return (
            <div className="post comments">
                <div className="post_head">
                    <div className="avater">
                        <img src={avatar} alt={`Аватарка пользоыателя ${name}`}/>
                        {name}
                    </div>
                    <button className="del" >Удалить</button>
                </div>
                <div className="post_body">
                    <p>
                        {text}
                    </p>
                </div> 
                <div className="post_footer">
                    <div className="date mlauto">
                        Дата публикации:  {this.getDate(date)}
                    </div>
                </div>
            </div>
        )
    }
}
