import React from 'react';
import './assets/sass/main.sass';
import EventCurrent from './components/pages/EventCurrent';
import AddEvaent from './container/AddEvaent';
import { BrowserRouter, Switch, Route, NavLink } from 'react-router-dom';
import Home from './components/pages/Home';
import EventNearest from './components/pages/EventNearest';
import EventPast from './components/pages/EventPast';
import OneEvent  from './components/pages/OneEvent';

const App: React.FC = () => {
  return (
    <BrowserRouter>
      <>
        <header className="header">
          <div className="container">
            <div className="row">
              <nav className="nav-bar">
                <ul className="nav-bar-menu">
                  <li><NavLink activeClassName="active" to="/past">Прошедшие</NavLink></li>
                  <li><NavLink activeClassName="active" to="/nearest">Текущие</NavLink></li>
                  <li><NavLink activeClassName="active" to="/current">Ближайшие</NavLink></li>
                </ul>
              </nav>
              <form className="serch">
                <input type="text" placeholder="Поиск..."/>
              </form>
            </div>
          </div>
        </header>
        <Switch>
          <Route exact path="/"> 
            <Home/>
          </Route>
          <Route path="/past">
            <EventPast/>
          </Route>
          <Route path="/nearest">
            <EventNearest/>
          </Route>
          <Route path="/current">
            <EventCurrent/>
          </Route>
          <Route path="/addevent">
            <AddEvaent/>
          </Route>
          <Route path="/event/:id" component={OneEvent} />
        </Switch>
      </>
    </BrowserRouter>
  );
}

export default App;
