import React from 'react'

interface Props {
    
}

const Home: React.FC<Props> = () => {
    return (
        <div className="container">
            <div className="row text-center">
                <h1>Список событий</h1>
                <p>На данном сайте вы можите ознакомиться со всеми событиями.</p>
                <p>Так же вы можите оставить свой коментарий к каждому событию.</p>
            </div>
        </div>
    )
}

export default Home
