import React, { Component } from 'react'
import { Dispatch } from 'redux'
import { connect } from 'react-redux'
import { IActionType } from '../../common'
import { Actions } from '../../actions/Actions'
import { IDispatchProps, IStoreState, IStoreEvent } from '../../actions/Model'
import EventList from '../../container/EventList'
import Loading from '../../container/Loading'
import ControlsBtn from '../../container/ControlsBtn'

// Текущие событие
type IProps = IStoreEvent & IDispatchProps

class EventCurrent extends Component<IProps,{}> {

    componentDidMount () {
        this.props.actions.getAllPost()
        //  this.props.actions.addEvent({description: "Lorem ipsum dolor sit amet consectetur adipisicing elit. Voluptatibus reiciendis ipsa omnis?", dete: "2020-11-22T17:35:12.410Z", important: true, title: "Title"})
    }

    render() {
        console.log(this.props.eventList);
        let template = this.props.eventList.map((item)=>{
            return (
                <EventList
                    key={item.id}
                    comments={item.comments}
                    description={item.description}
                    dete={item.dete}
                    id={item.id}
                    important={item.important}
                    title={item.title}
                    user={item.user}
                    userId={item.userId}
                    delete={this.props.actions.delEvent}
                />
            )
        })
        return (
            <>
                <ControlsBtn/>
                <div className="container mt-3">
                    <div className="row">
                        {this.props.loading?
                            <Loading/>
                        :
                            <>{template}</>
                        }
                    </div>
                </div>
            </>
        )
    }
}


const mapStateToProps = (state: IStoreState): IStoreEvent => ({
    eventList: state.event.eventList,
    loading: state.event.loading,
    error: state.event.error
})

const mapDispatchToProps = (dispatch: Dispatch<IActionType>): IDispatchProps => ({
    actions: new Actions(dispatch)
})

export default connect(mapStateToProps, mapDispatchToProps)(EventCurrent)