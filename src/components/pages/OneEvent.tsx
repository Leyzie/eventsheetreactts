import React, { Component } from 'react'
import { Dispatch } from 'redux'
import { connect } from 'react-redux'
import { IStoreState, IStoreOne, IDispatchProps } from '../../actions/Model'
import { IActionType } from '../../common'
import { Actions } from '../../actions/Actions'
import { RouteComponentProps } from 'react-router-dom'

interface IState {
    id?: string
}

type IProps = IStoreOne & IDispatchProps & RouteComponentProps<{id: string}>

class OneEvent extends Component<IProps,IState> {

    constructor(props: IProps) {
        super(props)
        this.state = {
        }
    }

    componentDidMount () {
        if (this.props.match.params.id) {
            const id: string = this.props.match.params.id
            this.setState({ id });
            this.props.actions.getOnePost(id)
        }
        
    }
    getDate = (value: string) => {
        let options = {
            day: 'numeric',
            month: 'numeric',
            year: 'numeric'
        }
        var date = new Date(value);
        return date.toLocaleString('ru', options)
    }
    render() {
        console.log(this.props);
        console.log(this.state.id);
        
        return (
            <>
                <p>asdasd {this.state.id}</p>
            </>
        )
    }
}

const mapStateToProps = (state: IStoreState): IStoreOne => ({
    oneEvent: state.oneEvent.oneEvent,
    loading: state.oneEvent.loading,
    error: state.oneEvent.error
})
const mapDispatchToProps = (dispatch: Dispatch<IActionType>): IDispatchProps => ({
    actions: new Actions(dispatch)
})
export default connect(mapStateToProps, mapDispatchToProps)(OneEvent)